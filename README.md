# WeATaViX: WEarable Actuated TAngibles forVIrtual reality eXperiences

Welcome to the **WeATaViX** Gitlab repository. WeATaViX is an interaction device designed for virtual reality. It provides a natural grasp feeling to the user.

[![WeATaViX Presentation & demo](https://img.youtube.com/vi/59fW1zRtaAo/0.jpg)](https://www.youtube.com/watch?v=59fW1zRtaAo)

### Assembly

WeATaViX is made up of 2 different parts (7 printed parts) assembled with standard M3 / M4 screws. You can print the parts yourself (.stl models are in CAO folder), and purchase everything needed for less than 50€.

Follow the instructions in the [documentation ](./WeATaViX_documentation.pdf) to assemble your own device.

### Mechanical Calibration

Once assembled, you need to calibrate your servo-motor and record the matching PWM value for engaged and disengaged states. The servo-motors are driven by a USB servo controller called [Pololu Maestro](https://www.pololu.com/category/102/maestro-usb-servo-controllers).

Every step of the calibration process is described in the [documentation ](./WeATaViX_documentation.pdf).


### Software Calibration

In the WeATaViX prefab, you'll find a calibration method using the vive controller.


## Documentation

You'll find the [WeATaViX_documentation.pdf](./WeATaViX_documentation.pdf) here.

## Licencing
![CC](./cc.png)

WeATaViX's Hardware (c) by Xavier de Tinguy, Thomas Howard & Guillaume Gicquel is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.

You should have received a copy of the license along with this work.  If not, see it [here](http://creativecommons.org/licenses/by-sa/3.0/).

![GPL](./gpl.png)

WeATaViX is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License. WeATaViX is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with WeATaViX.  If not, see it [here](https://www.gnu.org/licenses/gpl-3.0.en.html).
