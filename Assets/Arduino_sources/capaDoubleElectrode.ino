#include <CapacitiveSensor.h>

/*
 * CapitiveSense Library Demo Sketch
 * Paul Badger 2008
 * Uses a high value resistor e.g. 10M between send pin and receive pin
 * Resistor effects sensitivity, experiment with values, 50K - 50M. Larger resistor values yield larger sensor values.
 * Receive pin is the sensor pin - try different amounts of foil/metal on this pin
 */


CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4,6);        // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
//330kOhm chez nous
int etat;
long total1;
long total2;

void setup() {
   //cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
   Serial.begin(115200);
   etat = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  handleSerial();
  switch(etat)
  {
    case 0:
      total1 =  cs_4_2.capacitiveSensor(30);
      total2 =  cs_4_6.capacitiveSensor(30);
      break;

    case 1:
      Serial.print((int)total1);
      Serial.print("/");
      Serial.println((int)total2);
      etat = 0;
      break;
  }
}


void handleSerial() {
  if (Serial.available() > 0) {
    char incomingCharacter = Serial.read();
    switch (incomingCharacter)
    {
      case 'a':
        etat = 1;
        break;

      default:
        break;
    }
  }
}
