﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuiceMaster : MonoBehaviour
{
    public List<GameObject> apples = new List<GameObject>();
    public Transform lever;
    public Transform spawnCupSpot;
    public GameObject cupPrefab;
    public GameObject door;
    public AudioSource audioSource;
    public AudioClip blendingSound;
    public AudioClip emptySound;
    public Stream juiceStream;
    public GameObject currentCup;

    private bool isPouring = false;
    private bool isEmpty = true;

    private void OnTriggerStay(Collider other) {
        //if (other.GetComponent<CatchableObject>() != null && !other.GetComponent<CatchableObject>().isAttached) {
        //    if (this.apples.Count == 0 || !this.apples.Contains(other.gameObject)) {
        //        this.apples.Add(other.gameObject);
        //        this.isEmpty = false;
        //    }
        //}
    }

    private void Update() {
        if (this.apples.Count == 0) {
            this.isEmpty = true;
        } else {
            this.isEmpty = false;
        }

        if (this.spawnCupSpot.childCount == 0) {
            this.currentCup = null;
        }
    }

    public void pourJuice() {
        if (this.isEmpty) {
            this.stopPouring();
            if (!this.audioSource.isPlaying) {
                this.audioSource.PlayOneShot(this.emptySound);
            }
        } else {
            if (!this.isPouring) {
                if (this.spawnCupSpot.childCount == 0) {
                    this.currentCup = Instantiate(this.cupPrefab, this.spawnCupSpot, false);
                }

                this.isPouring = true;
                this.currentCup.GetComponent<Cup>().startFilling();
                this.StartCoroutine(this.makeJuice());
                this.juiceStream.startPouring();
            }
        }
    }

    public void stopPouring() {
        if (this.isPouring) {
            this.isPouring = false;
            this.StopCoroutine(this.makeJuice());
            this.StopAllCoroutines();
            this.currentCup.GetComponent<Cup>().stopFilling();
            this.juiceStream.stopPouring();
            this.audioSource.Stop();
        }
    }

    IEnumerator makeJuice() {

        this.audioSource.PlayOneShot(this.blendingSound);

        for (int i = 0; i < this.apples.Count; i++) {
            yield return new WaitForSeconds(2.0f);
            this.apples[i].SetActive(false);
        }

        foreach (GameObject apple in this.apples) {
            Destroy(apple);
        }

        this.apples.Clear();
        this.stopPouring();
    }
}
