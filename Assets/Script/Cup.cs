﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Cup : WeatavixInteractable
{
    public AudioSource audioSource;
    public GameObject liquid;
    public GameObject handle;
    public float timeToFill = 2.0f;

    private Vector3 maxScale = new Vector3(0.04f, 0.0002f, 0.04f);
    private Vector3 minScale = new Vector3(0.03f, 0.009f, 0.03f);
    private Vector3 minHeight = new Vector3(0, 0.0042f, 0);
    private Vector3 maxHeight = new Vector3(0, 0.0512f, 0);

    private int sloshSpeed = 60;
    private int rotateSpeed = 15;
    private int difference = 25;
    private Coroutine lastRoutine = null;

    protected override void Update() {
        base.Update();
        this.slosh();
        this.liquid.transform.Rotate(Vector3.up * this.rotateSpeed * Time.deltaTime, Space.Self);
    }

    protected override void Start() {
        base.Start();
        this.liquid.SetActive(false);
    }

    private void slosh() {
        //Inverse cup rotation
        Quaternion inverseRotation = Quaternion.Inverse(this.transform.rotation);

        //rotate to
        Vector3 finalRotation = Quaternion.RotateTowards(this.liquid.transform.localRotation, inverseRotation, this.sloshSpeed * Time.deltaTime).eulerAngles;

        //clamp
        finalRotation.x = this.clampRotationValue(finalRotation.x);
        finalRotation.z = this.clampRotationValue(finalRotation.z);

        this.liquid.transform.localEulerAngles = finalRotation;
    }

    private float clampRotationValue(float value) {
        float returnValue = 0.0f;

        if (value > 180.0f) {
            returnValue = Mathf.Clamp(value, 360.0f - this.difference, 360.0f);
        } else {
            returnValue = Mathf.Clamp(value, 0.0f, this.difference);
        }

        if (value < 360.0f - this.difference && value > this.difference) {
            this.spill();
        }

        return returnValue;
    }

    public void startFilling() {
        this.liquid.transform.localPosition = this.minHeight;
        this.liquid.SetActive(true);
        this.audioSource.Play();
        this.lastRoutine = this.StartCoroutine(this.raiseLiquid());
    }

    public void stopFilling() {
        this.StopCoroutine(this.lastRoutine);
        this.audioSource.Stop();
    }

    void spill() {
        this.liquid.SetActive(false);
    }

    IEnumerator raiseLiquid() {
        float t = 0f;

        while (t < this.timeToFill) {
            t += Time.deltaTime / this.timeToFill;
            this.liquid.transform.localPosition = Vector3.Lerp(this.minHeight, this.maxHeight, t);
            this.liquid.transform.localScale = Vector3.Lerp(this.minScale, this.maxScale, t);
            yield return null;
        }
    }
}
