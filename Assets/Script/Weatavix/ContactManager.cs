﻿using System;
using System.Collections;
using System.IO.Ports;
using System.Threading;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Class handling proximity contacts with the Weatavix's actuated sphere and the serial com with the Arduino.
 */

public class ContactManager : MonoBehaviour {
    public string arduinoCOM;
    public int contactThreshold;
    public bool looping = true;

    public float fingersCapacityValue, palmCapacitiveValue, maxFingerCapValue, maxPalmCapValue, minFingerCapValue, minPalmCapValue, thresholdFingers, thresholdPalm;

    private SerialPort stream;
    private Thread thread;

    [Range(0f, 1.0f)]
    public float palmNorm;
    [Range(0f, 1.0f)]
    public float fingersNorm;

    public bool IsLooping() {
        lock (this) {
            return this.looping;
        }
    }
    
    private void Start() {
        //Run communication thread
        this.thread = new Thread(this.threadLoop);
        this.thread.Start();
        Weatavix.instance.grabPinchAction.RemoveAllListeners(SteamVR_Input_Sources.RightHand);
    }

    private void Update() {
        //Get skeleton poser of attached object if exist
        if (Weatavix.instance.currentAttachedObject != null) {
            SteamVR_Skeleton_Poser poser = Weatavix.instance.currentAttachedObject.GetComponent<SteamVR_Skeleton_Poser>();

            this.getNormalCapacityValue(HandPart.Palm);
            this.getNormalCapacityValue(HandPart.Fingers);

            if (poser != null && poser.GetBlendingBehaviour("SqueezeFingers") != null) {
                //Set value between 0 & 1 for capacity values and feed it to skeleton poser blend value
                poser.SetBlendingBehaviourValue("SqueezeFingers", this.fingersNorm);
                poser.SetBlendingBehaviourValue("SqueezeThumb", this.palmNorm);
            }
        }
    }

    private void LateUpdate() {
        if (this.fingersCapacityValue > this.thresholdFingers || this.palmCapacitiveValue > this.thresholdPalm) {
            Weatavix.instance.isGrabbed = true;
        } else {
            Weatavix.instance.isGrabbed = false;
        }
    }

    private void getNormalCapacityValue(HandPart handPart) {
        switch (handPart) {
            case HandPart.Fingers:
            this.fingersCapacityValue = Mathf.Clamp(this.fingersCapacityValue, this.minFingerCapValue, this.maxFingerCapValue);
            this.fingersNorm = 1.0f - Mathf.Clamp((this.fingersCapacityValue - this.minFingerCapValue) / (this.maxFingerCapValue - this.minFingerCapValue), 0, 1);
            break;

            case HandPart.Palm:
            this.palmCapacitiveValue = Mathf.Clamp(this.palmCapacitiveValue, this.minPalmCapValue, this.maxPalmCapValue);
            this.palmNorm = 1.0f - Mathf.Clamp((this.palmCapacitiveValue - this.minPalmCapValue) / (this.maxPalmCapValue - this.minPalmCapValue), 0, 1);
            break;

            default:
            break;
        }
    }

    public void threadLoop() {
        this.connectToArduino();

        // Looping
        while (this.IsLooping()) {
            //Don't know why, but we have to write a character to the arduino serial port to be able to read it afterwise...
            this.writeToArduino("a");

            // Read from Arduino
            string result = this.readFromArduino();

            if (result != null) {
                var sArray = result.Split('/');
                this.fingersCapacityValue = int.Parse(sArray[0]);
                this.palmCapacitiveValue = int.Parse(sArray[1]);
                this.stream.DiscardInBuffer();
            }
        }

        Debug.Log("stream closing");
        this.stream.Close();
    }

    public void connectToArduino() {
        if (int.Parse(this.arduinoCOM) >= 0) {
            this.stream = new SerialPort("COM" + this.arduinoCOM, 115200);
            this.stream.ReadTimeout = 50;

            try {
                this.stream.Open();
                Debug.Log("Communication port with Arduino is ready.");
            } catch (Exception e) {
                Debug.LogError(e.Message);
                throw;
            }
        } else {
            Debug.Log("Unable to open communication port with Arduino.");
        }
    }

    public string readFromArduino(int timeout = 50) {
        try {
            return this.stream.ReadLine();
        } catch (TimeoutException e) {
            return null;
        } catch (Exception e) {
            Debug.Log(e.Message);
            return null;
        }
    }

    public void writeToArduino(string message) {
        if (this.stream != null) {
            this.stream.WriteLine(message);
            this.stream.BaseStream.Flush();
        }
    }

    public void stopThread() {
        lock (this) {
            this.looping = false;
        }
    }

    private void OnApplicationQuit() {
        this.stopThread();
    }
}

public enum HandPart
{
    Fingers, Palm
}
