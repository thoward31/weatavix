﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Manager script for Weatavix (Singleton)
 */

[RequireComponent(typeof(ServoManager))]
[RequireComponent(typeof(ContactManager))]
[RequireComponent(typeof(FlickDetector))]
public class Weatavix : Hand {
    [HideInInspector]
    public static Weatavix instance;
    public event OnInteractionStateChangeDelegate OnInteractionStateChange;
    public delegate void OnInteractionStateChangeDelegate(InteractionState newState);

    [Range(0.05f, 0.5f)]
    public float maxContactDistance;
    [Range(0f, 0.1f)]
    public float maxDistanceThreshold;
    [Range(-0.1f, 0.1f)]
    public float zBackHandThreshold;
    [Range(0f, 0.1f)]
    public float lateralScale;
    [Range(0f, 0.1f)]
    public float releaseDelay;
    [Range(0f, 0.5f)]
    public float smoothTime;
    public bool isGrabbed = false, isReleased = true;
    public AnimationCurve parabolaCurve;
    public GameObject distantHovering;

    [HideInInspector]
    public InteractionState currentInteractionState = InteractionState.SEARCH_TARGETS;
    private InteractionState lastInteractionState = InteractionState.SEARCH_TARGETS;

    private float commandServo;
    private float tempCommandServo;
    private float timerTransition;
    private float transitionTime;
    private bool boolTransitionEnabled;
    private Vector3 estimatedPosition;

    private ServoManager servoManager;
    private ContactManager contactManager;
    private List<Vector3> handLastPositions = new List<Vector3>();
    private Vector3 lastHandPosition;
    private Vector3 handMeanSpeed;
    private float normalScale = 1.0f;
    private float timerRelease;
    private bool isDistantHoveringLocked = false;

    protected override void Awake() {
        //Singleton declaration
        if (Weatavix.instance != null) {
            GameObject.Destroy(this);
        } else {
            instance = this;
            GameObject.DontDestroyOnLoad(this);
        }

        this.servoManager = this.GetComponent<ServoManager>();
        this.contactManager = this.GetComponent<ContactManager>();
        this.GetComponent<FlickDetector>().OnBegin.AddListener(this.hasFlick);
        this.GetComponent<FlickDetector>().OnEnd.AddListener(this.flickEnded);
        base.Awake();
    }

    void LateUpdate() {
        this.updateHandSpeed();

        switch (this.currentInteractionState) {
            case InteractionState.SEARCH_TARGETS:
            this.currentInteractionState = this.searchTarget();
            break;

            case InteractionState.GRASPED:
            this.currentInteractionState = this.grasped();
            break;

            case InteractionState.RELEASE_GRASP:
            this.currentInteractionState = this.releaseGrasp();
            break;

        }

        //If interaction state has changed
        if (this.currentInteractionState != this.lastInteractionState) {
            //memorize current interaction state
            this.lastInteractionState = this.currentInteractionState;
            //if something registered to the state changing event, fires it
            OnInteractionStateChange?.Invoke(this.currentInteractionState);
        }

        //update servo command
        if (this.boolTransitionEnabled) {
            this.timerTransition += Time.deltaTime;
            float alpha = this.timerTransition / this.transitionTime;
            if (alpha >= 1f) {
                this.updateServo(this.commandServo);
                this.boolTransitionEnabled = false;
            } else {
                this.updateServo((1 - alpha / this.transitionTime) * this.tempCommandServo + alpha * this.commandServo);
            }
        } else {
            this.updateServo(this.commandServo);
        }
    }

    //Calculate mean speed of the hand based on the 5 last frames positions
    private void updateHandSpeed() {
        int nbFrames = 5;

        this.handLastPositions.Add((this.transform.position - this.lastHandPosition) / Time.deltaTime);
        if (this.handLastPositions.Count > nbFrames) this.handLastPositions.RemoveAt(0);
        this.handMeanSpeed = Vector3.zero;

        foreach (Vector3 posiion in this.handLastPositions) {
            this.handMeanSpeed += posiion;
        }

        this.handMeanSpeed /= this.handLastPositions.Count;
        this.lastHandPosition = this.transform.position;
    }

    private void updateServo(float distanceV) {
        if (distanceV > this.maxDistanceThreshold) {
            this.servoManager.updateServo(1);
        } else if (distanceV >= 0) {
            this.servoManager.updateServo(distanceV / this.maxDistanceThreshold);
        }
    }

    private void SmoothTransitionServo(float tempCommand, float transitionTime) {
        this.tempCommandServo = tempCommand;
        this.transitionTime = transitionTime;
        this.timerTransition = 0f;
        this.boolTransitionEnabled = true;
    }

    //Actuator is grasped by the user between fingers & palm
    private InteractionState grasped() {
        this.commandServo = 0;

        //If the user releases grasp during the grasped state
        if (!this.isGrabbed) {
            this.timerRelease = 0;
            this.estimatedPosition = Vector3.zero;
            return InteractionState.RELEASE_GRASP;
        }

        return InteractionState.GRASPED;
    }

    //User has released the grasp on the actuator
    private InteractionState releaseGrasp() {
        this.DetachObject(this.currentAttachedObject);
        float scoreDistanceEstimated;

        if (this.hoveringInteractable != null && this.hoveringInteractable.GetComponent<WeatavixCircularDrive>() != null) {
            this.hoveringInteractable.GetComponent<WeatavixCircularDrive>().detach();
        }

        //If the user grasps it again
        if (this.isGrabbed) {
            return InteractionState.GRASPED;
        }

        this.estimatedPosition += this.hoverSphereTransform.transform.InverseTransformVector(this.handMeanSpeed) * Time.deltaTime;
        this.timerRelease += Time.deltaTime;

        //Estimate the command to send to the servo, but disengage if the target is behind the user's hand
        if (this.estimatedPosition.z >= 0) {
            scoreDistanceEstimated = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(this.estimatedPosition.x, 2) + Mathf.Pow(this.estimatedPosition.y, 2)) + this.normalScale * Mathf.Pow(this.estimatedPosition.z, 2));
        } else if (this.estimatedPosition.z > -this.zBackHandThreshold) {
            scoreDistanceEstimated = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(this.estimatedPosition.x, 2) + Mathf.Pow(this.estimatedPosition.y, 2)));
        } else {
            scoreDistanceEstimated = this.maxDistanceThreshold;
        }

        this.commandServo = scoreDistanceEstimated;

        //If the user has released the object in the timeframe, consider it released and switch interaction state to idle
        if (this.timerRelease > this.releaseDelay) {
            this.SmoothTransitionServo(this.commandServo, this.smoothTime);
            this.servoManager.setVariation(0);

            return InteractionState.SEARCH_TARGETS;
        }

        return InteractionState.RELEASE_GRASP;
    }

    private InteractionState searchTarget() {
        Vector3 localFramePosition;

        //If an object has been defined as the closest
        if (this.hoveringInteractable != null) {
            float scoreDistance;
            this.GetComponent<BeziersCurveLine>().active = false;
            this.distantHovering = null;


            if (this.hoveringInteractable.GetComponent<WeatavixCircularDrive>() != null) {

                if (this.hoveringInteractable.GetComponent<WeatavixCircularDrive>().childCollider != null) {
                    localFramePosition = this.hoverSphereTransform.transform.InverseTransformPoint(this.hoveringInteractable.GetComponent<WeatavixCircularDrive>().childCollider.transform.position);
                } else {
                    localFramePosition = this.hoverSphereTransform.transform.InverseTransformPoint(this.hoveringInteractable.transform.position);
                }

            } else {

                if (this.hoveringInteractable.GetComponent<Throwable>() != null && this.hoveringInteractable.GetComponent<Throwable>().attachmentOffset != null) {
                    localFramePosition = this.hoverSphereTransform.transform.InverseTransformPoint(this.hoveringInteractable.GetComponent<Throwable>().attachmentOffset.transform.position);
                } else {
                    localFramePosition = this.hoverSphereTransform.transform.InverseTransformPoint(this.hoveringInteractable.transform.position);
                }
            }

            //Define the approximated position to send to the servo
            if (localFramePosition.z >= 0) {
                //close
                scoreDistance = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(localFramePosition.x, 2) + Mathf.Pow(localFramePosition.y, 2)) + this.normalScale * Mathf.Pow(localFramePosition.z, 2));
            } else if (localFramePosition.z > this.zBackHandThreshold) {
                //very close
                scoreDistance = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(localFramePosition.x, 2) + Mathf.Pow(localFramePosition.y, 2)));
            } else {
                //far away
                scoreDistance = this.maxDistanceThreshold;
            }

            this.commandServo = scoreDistance;

            if ((scoreDistance < this.maxContactDistance) && this.isGrabbed) {
                //close enough
                if (this.currentAttachedObject == null) {
                    if (this.hoveringInteractable.GetComponent<WeatavixCircularDrive>() != null) {
                        this.hoveringInteractable.GetComponent<WeatavixCircularDrive>().attach(this);
                        AttachmentFlags flags = AttachmentFlags.VelocityMovement | AttachmentFlags.TurnOffGravity;
                        this.AttachObject(this.hoveringInteractable.gameObject, GrabTypes.Pinch, flags);
                    } else {
                        this.hoveringInteractable.transform.SetParent(null);
                        this.hoveringInteractable.GetComponent<Rigidbody>().useGravity = true;
                        this.hoveringInteractable.GetComponent<Rigidbody>().isKinematic = false;
                        this.AttachObject(this.hoveringInteractable.gameObject, GrabTypes.Pinch, this.hoveringInteractable.GetComponent<Throwable>().attachmentFlags);
                    }
                }

                return InteractionState.GRASPED;
            }
        } else {
            //Cast a spherecast to the normal direction of the palm against the interactable layer
            //to find distant intertactables
            RaycastHit hit;
            int layerMask = 1 << 11;

            Debug.DrawRay(this.hoverSphereTransform.position, this.hoverSphereTransform.TransformDirection(Vector3.left), Color.red);

            if (Physics.SphereCast(this.hoverSphereTransform.position, 0.4f, this.hoverSphereTransform.TransformDirection(Vector3.left), out hit, 3.0f, layerMask) && !this.isDistantHoveringLocked) {
                GameObject hitted = hit.collider.gameObject;

                if (hitted.GetComponent<Throwable>() != null &&
                    hitted.GetComponent<Rigidbody>().velocity.magnitude < 0.5f &&
                    !hitted.GetComponent<Interactable>().attachedToHand &&
                    Vector3.Distance(hitted.transform.position, this.hoverSphereTransform.position) > 0.75f) {

                    this.StopCoroutine(this.forgetDistantHovering());
                    this.isDistantHoveringLocked = false;
                    this.GetComponent<BeziersCurveLine>().start = this.hoverSphereTransform.position;
                    this.GetComponent<BeziersCurveLine>().end = hitted.transform.position;
                    this.GetComponent<BeziersCurveLine>().active = true;
                    this.distantHovering = hitted;
                }
            } else {
                this.StartCoroutine(this.forgetDistantHovering());
            }

            this.commandServo = this.maxDistanceThreshold;
        }

        return InteractionState.SEARCH_TARGETS;
    }

    private void hasFlick() {
        this.isDistantHoveringLocked = true;
        this.StartCoroutine(this.forgetDistantHovering());
    }

    private void flickEnded() {
        if (this.distantHovering != null) {
            this.StartCoroutine(this.smoothTranslateDistantHovering(this.distantHovering));
        }
    }

    protected override void Update() {
        this.UpdateNoSteamVRFallback();

        GameObject attachedObject = this.currentAttachedObject;
        if (attachedObject != null) {
            //attachedObject.SendMessage("HandAttachedUpdate", this, SendMessageOptions.DontRequireReceiver);
        }

        if (this.hoveringInteractable) {
            this.hoveringInteractable.SendMessage("HandHoverUpdate", this, SendMessageOptions.DontRequireReceiver);
        }

        if (this.distantHovering != null) {
            Vector3 velocity = this.GetComponentInParent<SteamVR_Behaviour_Pose>().GetAngularVelocity();
            this.GetComponent<FlickDetector>().checkFlick(velocity.magnitude);
        }
    }

    public IEnumerator smoothTranslateDistantHovering(GameObject distantInteractable) {
        float timeElapsed = 0f;
        float totalTime = 0.5f;
        Vector3 smoothPosition;
        Vector3 originalPos = distantInteractable.transform.position;

        distantInteractable.transform.SetParent(null);
        distantInteractable.GetComponent<Rigidbody>().useGravity = false;
        distantInteractable.GetComponent<Rigidbody>().isKinematic = true;

        while (timeElapsed < totalTime) {

            float t = Mathf.Lerp(0, 1, timeElapsed / totalTime);
            float curveSample = this.parabolaCurve.Evaluate(t);
            smoothPosition = Vector3.Lerp(originalPos, this.hoverSphereTransform.position, timeElapsed / totalTime);

            distantInteractable.transform.position = smoothPosition + new Vector3(0, curveSample, 0);

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(.5f);

        this.isDistantHoveringLocked = false;

        if (!this.ObjectIsAttached(distantInteractable)) {
            distantInteractable.GetComponent<Rigidbody>().useGravity = true;
            distantInteractable.GetComponent<Rigidbody>().isKinematic = false;
        }

        yield return null;
    }

    protected IEnumerator forgetDistantHovering() {
        yield return new WaitForSeconds(.3f);
        this.GetComponent<BeziersCurveLine>().active = false;
        this.distantHovering = null;
        this.isDistantHoveringLocked = false;
        yield return null;
    }

    public IEnumerator smoothOpeningHand() {
        float timeElapsed = 0f;
        float totalTime = 0.2f;
        float smoothValue;

        this.StopCoroutine(this.smoothClosingHand());

        while (timeElapsed < totalTime) {

            smoothValue = Mathf.Lerp(0, 1, timeElapsed / totalTime);
            this.skeleton.fallbackPoser.SetBlendingBehaviourValue("hovering", smoothValue);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        this.skeleton.fallbackPoser.SetBlendingBehaviourValue("hovering", 1.0f);
        yield return null;
    }

    public IEnumerator smoothClosingHand() {
        float timeElapsed = 0f;
        float totalTime = 0.3f;
        float smoothValue;

        this.StopCoroutine(this.smoothOpeningHand());

        while (timeElapsed < totalTime) {

            smoothValue = Mathf.Lerp(1, 0, timeElapsed / totalTime);
            if (this.skeleton != null && this.skeleton.fallbackPoser != null) {
                this.skeleton.fallbackPoser.SetBlendingBehaviourValue("hovering", smoothValue);
            }
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        if (this.skeleton != null && this.skeleton.fallbackPoser != null) {
            this.skeleton.fallbackPoser.SetBlendingBehaviourValue("hovering", 0f);
        }
        yield return null;
    }
}








public enum InteractionState {
    SEARCH_TARGETS, RELEASE_GRASP, GRASPED
}
