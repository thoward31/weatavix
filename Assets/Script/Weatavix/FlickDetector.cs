﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/**
    05/07/2021 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    Class to detect flick movement from the hand.
**/

public class FlickDetector : MonoBehaviour
{
    //Threshold that triggers the beginning of the flick
    public float beginThreshold = 5.25f;
    //Threshold that triggers the end of the flick
    public float endThreshold = 2.25f;

    //Trigger events
    public UnityEvent OnBegin = new UnityEvent();
    public UnityEvent OnEnd = new UnityEvent();

    //bool to avoid calling event if already flicking
    private bool brokenThreshold = false;

    public void checkFlick(float speed) {
        if (this.hasFlickBegun(speed)) {
            this.OnBegin.Invoke();
            this.brokenThreshold = true;
        }

        if (this.hasFlickEnded(speed)) {
            this.OnEnd.Invoke();
            this.brokenThreshold = false;
        }
    }

    private bool hasFlickBegun(float speed) {
        return this.brokenThreshold ? true : (speed > this.beginThreshold);
    }

    private bool hasFlickEnded(float speed) {
        return this.brokenThreshold ? (speed < this.endThreshold) : false;
    }
}
