﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.Extras;
using Valve.VR.InteractionSystem;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Calibration script for the Weatavix mounted device
 */

public class Calibration : MonoBehaviour {

    //Class handling reading / writing in the .txt file
    public TextFileParser parser;
    //Text displayed in calibration panel UI
    public Text text;
    //UI buttons for calibration instructions
    public Button btnOK, btnStop;
    //Menu button trigger
    public SteamVR_Action_Boolean menuTrigger;
    public Camera camVR;
    //Transform containing both canvases
    public GameObject canvases;
    public Image img;
    public GameObject controllerModel;
    //SteamVR controller (left hand)
    public SteamVR_Behaviour_Pose controller;

    private bool isCalibDone = false;
    private bool isCalibMenuOn = false;
    private bool isHandInitialized = false;
    //events fired by buttons
    private UnityEvent okEvent, stopEvent;


    private void Awake() {
        this.okEvent = new UnityEvent();
        this.stopEvent = new UnityEvent();

        this.canvases.SetActive(false);
        //Hide controller in left hand when available
        SteamVR_Events.RenderModelLoaded.AddListener(this.disableController);
    }

    private void Start() {
        this.stopCalibrationProcess();
    }

    private void Update() {
        if (this.menuTrigger.GetStateUp(SteamVR_Input_Sources.Any)) {
            if (!this.isCalibMenuOn) {
                this.startCalibrationProcess();
            }
        }
    }

    public void disableController(SteamVR_RenderModel model, bool success) {
        if (success && model.gameObject == this.controllerModel) {
            Player.instance.leftHand.HideController();
        }
    }

    //Callback from steamVR when virtual hand is spawned
    public void OnHandInitialized() {
        this.isHandInitialized = true;

        //quick replace
        this.transform.localPosition = new Vector3(0.0878f, -0.0583f, -0.0971f);
        this.transform.localEulerAngles = new Vector3(135f, -91f, 0);
        if (Player.instance.leftHand.isPoseValid) {
            this.controllerModel = Player.instance.leftHand.GetComponentInChildren<SteamVR_RenderModel>().gameObject;
        }

        //If the parser already has values stored in a txt file, calibrate the device with those values.
        //Otherwise, start the calibration process
        if (this.parser.hasValues) {
            this.calibrate(this.parser.getHandOffset(), this.parser.getHandRotation());
        } else {
            this.startCalibrationProcess();
        }
    }

    private void startCalibrationProcess() {
        //Set the panel text
        if (this.isCalibDone || this.isCalibMenuOn) {
            this.text.text = "Do you want to start calibration ?";
        } else {
            this.text.text = "Let's calibrate the Weatavix !";
        }

        //disable hands
        Player.instance.leftHand.Hide();
        Player.instance.rightHand.Hide();
        //display controller
        Player.instance.leftHand.ShowController();

        Player.instance.rightHand.GetComponent<HandPhysics>().enabled = false;

        if (this.controllerModel == null && Player.instance.leftHand.isPoseValid) {
            this.controllerModel = Player.instance.leftHand.GetComponentInChildren<SteamVR_RenderModel>().gameObject;
        }

        //Register laser pointer events
        this.controller.GetComponent<SteamVR_LaserPointer>().enabled = true;

        if (this.controller.GetComponent<SteamVR_LaserPointer>().holder != null) {
            this.controller.GetComponent<SteamVR_LaserPointer>().holder.gameObject.SetActive(true);
        }

        this.controller.GetComponent<SteamVR_LaserPointer>().PointerIn += this.PointerInside;
        this.controller.GetComponent<SteamVR_LaserPointer>().PointerOut += this.PointerOutside;
        this.controller.GetComponent<SteamVR_LaserPointer>().PointerClick += this.PointerClick;

        this.isCalibDone = false;
        this.isCalibMenuOn = true;

        this.okEvent.AddListener(this.displayCalibInstructions);
        this.stopEvent.AddListener(this.stopCalibrationProcess);

        this.canvases.SetActive(true);
        this.canvases.transform.rotation = Quaternion.LookRotation(this.camVR.transform.forward, Vector3.up);
        Vector3 canvasHeight = this.canvases.transform.GetChild(0).position;
        this.canvases.transform.GetChild(0).position = new Vector3(canvasHeight.x, this.camVR.transform.position.y, canvasHeight.z);
        this.btnStop.gameObject.SetActive(true);
        this.btnOK.gameObject.SetActive(true);
    }

    private void stopCalibrationProcess() {
        this.okEvent.RemoveAllListeners();
        this.stopEvent.RemoveAllListeners();

        this.canvases.SetActive(false);
        this.btnStop.gameObject.SetActive(false);
        this.btnOK.gameObject.SetActive(false);
        this.isCalibMenuOn = false;

        Player.instance.leftHand.Show();
        Player.instance.rightHand.Show();
        Player.instance.leftHand.HideController();
        Player.instance.rightHand.GetComponent<HandPhysics>().enabled = true;

        if (this.controller != null) {
            this.controller.GetComponent<SteamVR_LaserPointer>().enabled = false;

            this.controller.GetComponent<SteamVR_LaserPointer>().PointerIn -= this.PointerInside;
            this.controller.GetComponent<SteamVR_LaserPointer>().PointerOut -= this.PointerOutside;
            this.controller.GetComponent<SteamVR_LaserPointer>().PointerClick -= this.PointerClick;
            if (this.controller.GetComponent<SteamVR_LaserPointer>().holder != null) {
                this.controller.GetComponent<SteamVR_LaserPointer>().holder.gameObject.SetActive(false);
            }
            if (this.transform.localPosition != Vector3.zero) {
                this.saveValues(this.transform.localPosition, this.transform.localRotation);
            }
        }
    }

    private void displayCalibInstructions() {
        this.okEvent.RemoveAllListeners();
        this.text.text = "Points are sampled with the controller.\n\n" +
            "Take the controller in your right hand and press the OK button." +
            "Press STOP button when it feels calibrated.";
        this.okEvent.AddListener(this.samplePosition);
    }

    private void samplePosition() {
        this.transform.localPosition = Vector3.zero;
        this.transform.localEulerAngles = Vector3.zero;

        this.transform.position = this.controllerModel.transform.position;
        this.transform.rotation = this.controllerModel.transform.rotation;
        Player.instance.rightHand.Show();
    }

    private void saveValues(Vector3 localPos, Quaternion localRot) {
        this.parser.setValues(localPos, localRot);
        this.parser.saveValues();
    }

    private void calibrate(Vector3 localPos, Quaternion localRot) {
        this.transform.localPosition = localPos;
        this.transform.localRotation = localRot;
        this.isCalibDone = true;
    }



    // LASER POINTER EVENTS

    public void PointerClick(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            if (e.target.gameObject.GetComponent<Button>().interactable) {
                e.target.gameObject.GetComponent<Button>().onClick.Invoke();
            }
        }
    }

    public void PointerInside(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            EventSystem.current.SetSelectedGameObject(e.target.gameObject);
            e.target.gameObject.GetComponent<Button>().OnSelect(new BaseEventData(EventSystem.current));
        }
    }

    public void PointerOutside(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            EventSystem.current.SetSelectedGameObject(null);
            e.target.gameObject.GetComponent<Button>().OnDeselect(new BaseEventData(EventSystem.current));
        }
    }

    public void okClickEvent() {
        this.okEvent.Invoke();
    }

    public void stopClickEvent() {
        this.stopEvent.Invoke();
    }
}
