﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

/**
    05/07/2021 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    Class extending classic SteamVR interactable to add animations on hovering.
**/

public class WeatavixInteractable : Interactable
{
    protected override void OnHandHoverBegin(Hand hand) {
        base.OnHandHoverBegin(hand);

        if (Weatavix.instance.hoveringInteractable == this) {
            Weatavix.instance.StartCoroutine(Weatavix.instance.smoothOpeningHand());
        }
    }

    protected override void OnHandHoverEnd(Hand hand) {
        base.OnHandHoverEnd(hand);

        if (Weatavix.instance.hoveringInteractable == this) {
            Weatavix.instance.StartCoroutine(Weatavix.instance.smoothClosingHand());
        }
    }
}
