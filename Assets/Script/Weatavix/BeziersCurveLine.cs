﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *      Author : Guillaume Gicquel
 *      Date : 05/07/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Class handling the drawing of curve on distant hovering
 */

public class BeziersCurveLine : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public Vector3 start;
    public Transform mid;
    public Vector3 end;
    public bool active = false;

    void Start() {
        this.lineRenderer = this.GetComponent<LineRenderer>();
    }

    void Update() {
        if (this.active) {
            this.lineRenderer.enabled = true;
            this.DrawQuadraticBezierCurve(this.start, this.mid.position, this.end);
        } else {
            this.lineRenderer.enabled = false;
        }
    }

    void DrawQuadraticBezierCurve(Vector3 point0, Vector3 point1, Vector3 point2) {
        this.lineRenderer.positionCount = 200;
        float t = 0f;
        Vector3 B = new Vector3(0, 0, 0);
        for (int i = 0; i < this.lineRenderer.positionCount; i++) {
            B = (1 - t) * (1 - t) * point0 + 2 * (1 - t) * t * point1 + t * t * point2;
            this.lineRenderer.SetPosition(i, B);
            t += (1 / (float)this.lineRenderer.positionCount);
        }
    }
}
