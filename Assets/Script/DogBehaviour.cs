﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class DogBehaviour : MonoBehaviour
{
    public List<Transform> randomSpots;
    public Transform player;
    public float minTimeWaiting;
    public float maxTimeWaiting;
    public float maxTimeWaitingForPick;
    public SteamVR_Action_Boolean graspTrigger;
    public GameObject head;
    public GameObject mouth;
    public GameObject favoriteBall;
    public List<AudioClip> barkingSounds = new List<AudioClip>();
    public List<AudioClip> whiningSounds = new List<AudioClip>();
    public List<AudioClip> whistlingSounds = new List<AudioClip>();
    public AudioSource audioSource;
    public AudioSource playerAudioSource;

    private Transform currentDestination;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private int previousSpotIndex = 0;
    private float distanceToTarget = 1.5f;
    private bool isSitted = false;
    private bool isPlaying = false;
    private bool isSearching = false;
    private GameObject playingTarget;
    private GameObject ballInMouth;

    private const float defaultMouthOpening = -28.0f;
    private const float mouthOpened = -55.0f;
    private float lastEventTime = 0;

    private void Awake() {
        this.navMeshAgent = this.GetComponent<NavMeshAgent>();
        this.animator = this.GetComponent<Animator>();
    }

    void Start() {
        Weatavix.instance.OnInteractionStateChange -= this.catchEvent;
        Weatavix.instance.OnInteractionStateChange += this.catchEvent;
        this.goToRandomPoint();
    }

    void Update() {
        float angle = Vector3.Angle(this.navMeshAgent.desiredVelocity.normalized, this.transform.forward);

        if (this.navMeshAgent.desiredVelocity.normalized.x < this.transform.forward.x) {
            angle *= -1;
        }

        angle = (angle + 180.0f) % 360.0f;

        this.animator.SetFloat("Velocity", this.navMeshAgent.velocity.magnitude);
        this.animator.SetFloat("Direction", this.remap(angle, 0, 360, -1, 1));

        if (this.graspTrigger.GetStateUp(SteamVR_Input_Sources.Any)) {
            this.playerCalls();
        }

        if (this.ballInMouth != null) {
            this.mouth.transform.eulerAngles = new Vector3(mouthOpened, 0, 0);
        }
    }

    void randomStand(int index) {
        switch (index) {
            case 0:
            this.animator.ResetTrigger("Sits");
            this.animator.SetTrigger("Sits");
            this.isSitted = true;
            break;

            case 1:
            this.animator.ResetTrigger("Lies");
            this.animator.SetTrigger("Lies");
            this.isSitted = true;
            break;

            case 2:
            this.animator.SetTrigger("Stand");
            break;
        }
    }

    void bark() {
        this.audioSource.PlayOneShot(this.barkingSounds[Random.Range(0, this.barkingSounds.Count - 1)]);
    }

    void whine() {
        this.audioSource.PlayOneShot(this.whiningSounds[Random.Range(0, this.whiningSounds.Count - 1)]);
    }

    void whistle() {
        this.playerAudioSource.PlayOneShot(this.whistlingSounds[Random.Range(0, this.whistlingSounds.Count - 1)]);
    }

    public void catchEvent(InteractionState state) {

        if (Time.time - this.lastEventTime > 0.5f) {
            if (state == InteractionState.GRASPED) {
                if (this.ballInMouth != null && Player.instance.rightHand.ObjectIsAttached(this.ballInMouth)) {
                    this.playerTookBallFromMouth();
                } else {
                    this.playingTarget = Player.instance.rightHand.AttachedObjects[0].attachedObject;
                    if (this.isPlaying) {
                        this.playerPickedBall(Player.instance.rightHand.AttachedObjects[0].attachedObject);
                    }
                }
            } else if (state == InteractionState.RELEASE_GRASP && this.isPlaying) {
                if (this.isPlaying) {
                    this.playerThrownBall();
                } else {
                    this.playingTarget = null;
                }
            }
        }

        this.lastEventTime = Time.time;

    }

    void goToRandomPoint() {
        this.distanceToTarget = 0.2f;
        int index = Random.Range(0, 5);

        while (index == this.previousSpotIndex) {
            index = Random.Range(0, 5);
        }

        this.previousSpotIndex = index;

        int willBark = Random.Range(0, 1);
        if (willBark == 1) {
            this.bark();
        }

        this.currentDestination = this.randomSpots[index];
        this.StartCoroutine(this.goToRoutine());
    }

    void playerCalls() {
        this.whistle();
        this.isPlaying = true;
        this.StopAllCoroutines();
        this.animator.SetBool("isPlaying", true);

        if (Weatavix.instance.currentInteractionState == InteractionState.GRASPED) {
            if (this.isSitted) {
                this.StartCoroutine(this.waitForStandUp());
            } else {
                this.goToPlayer();
            }
        } else {
            this.isSearching = true;
            this.playingTarget = this.favoriteBall;

            if (this.isSitted) {
                this.StartCoroutine(this.waitForStandUp());
            } else {
                this.goFetch();
            }
        }
    }

    void goToPlayer() {
        this.distanceToTarget = 0.7f;
        this.currentDestination = this.player;
        this.StartCoroutine(this.goToRoutine());
    }

    public void playerPickedBall(GameObject target) {
        this.animator.ResetTrigger("Crouch");
        this.playingTarget = target;
        this.StopAllCoroutines();
        this.StopCoroutine(this.waitForPlayerPickOrThrow());
        this.animator.SetTrigger("Crouch");
        this.navMeshAgent.isStopped = true;
        this.navMeshAgent.SetDestination(this.transform.position);
        this.navMeshAgent.isStopped = false;
        this.currentDestination = target.transform;
        this.StartCoroutine(this.waitForPlayerPickOrThrow());
    }

    public void playerThrownBall() {
        this.animator.ResetTrigger("Stand");
        this.animator.SetTrigger("Stand");
        this.isSearching = true;
        this.bark();
        this.animator.ResetTrigger("looksDown");
        this.animator.SetTrigger("looksDown");
        this.goFetch();
    }

    void playerTookBallFromMouth() {
        this.attachBallToMouth(false);
        this.playerPickedBall(Player.instance.rightHand.AttachedObjects[0].attachedObject);
    }

    void goFetch() {
        this.distanceToTarget = 0.5f;
        this.currentDestination = this.playingTarget.transform;
        this.StartCoroutine(this.goToRoutine());
    }

    IEnumerator waitRoutine(float time = 0f) {
        this.animator.ResetTrigger("Stand");

        if (time == 0f) {
            time = Random.Range(this.minTimeWaiting, this.maxTimeWaiting);
        }

        yield return new WaitForSeconds(time);

        if (this.isSitted) {
            this.StartCoroutine(this.waitForStandUp());
        } else {
            if (this.isPlaying) {

            } else {
                this.goToRandomPoint();
            }
        }
    }

    IEnumerator goToRoutine() {
        Vector3 targetPos;

        if (this.currentDestination == this.player) {
            targetPos = this.player.GetComponent<Player>().feetPositionGuess;
        } else {
            targetPos = this.currentDestination.position;
        }

        float distance = Vector3.Distance(this.transform.position, targetPos);
        this.navMeshAgent.SetDestination(this.currentDestination.position);
        this.animator.SetBool("isMoving", true);

        while (distance > this.distanceToTarget) {
            this.navMeshAgent.SetDestination(this.currentDestination.position);
            targetPos = this.currentDestination.position;
            distance = Vector3.Distance(this.transform.position, targetPos);
            yield return null;
        }

        this.animator.SetBool("isMoving", false);

        if (this.isPlaying) {
            this.navMeshAgent.isStopped = true;
            this.navMeshAgent.SetDestination(this.transform.position);
            this.navMeshAgent.isStopped = false;

            if (this.isSearching) {
                this.StartCoroutine(this.pickBallAndGoToPlayer());
            } else {
                if (this.ballInMouth != null) {
                    this.giveBallAndWait();
                } else {
                    this.arrivedToPlayer();
                }
            }
        } else {
            int positionIndex = Random.Range(0, 3);
            this.randomStand(positionIndex);

            this.StartCoroutine(this.waitRoutine());
        }
    }

    IEnumerator waitForStandUp() {
        this.animator.SetTrigger("Stand");
        this.isSitted = false;
        yield return new WaitForSeconds(0.5f);

        if (this.isPlaying) {
            if (this.isSearching) {
                this.goFetch();
            } else {
                this.goToPlayer();
            }
        } else {
            this.goToRandomPoint();
        }
    }

    void arrivedToPlayer() {
        this.transform.LookAt(this.player.GetComponent<Player>().feetPositionGuess);

        if (Weatavix.instance.currentInteractionState == InteractionState.GRASPED) {
            this.animator.ResetTrigger("Crouch");
            this.animator.SetTrigger("Crouch");
        } else {
            this.animator.ResetTrigger("Sits");
            this.animator.SetTrigger("Sits");
            this.isSitted = true;
        }

        this.StartCoroutine(this.waitForPlayerPickOrThrow());
    }

    IEnumerator waitForPlayerPickOrThrow() {
        this.animator.ResetTrigger("Stand");
        this.animator.ResetTrigger("looksUp");
        this.animator.SetTrigger("looksUp");
        yield return new WaitForSeconds(3*(this.maxTimeWaitingForPick / 4));
        this.whine();
        this.animator.ResetTrigger("looksDown");
        this.animator.SetTrigger("looksDown");

        if (this.ballInMouth != null) {
            this.animator.ResetTrigger("isPicking");
            this.animator.SetTrigger("isPicking");
            yield return new WaitForSeconds(1.0f);

            this.attachBallToMouth(false);

            this.animator.ResetTrigger("Sits");
            this.animator.SetTrigger("Sits");
            this.isSitted = true;
            this.animator.ResetTrigger("looksUp");
            this.animator.SetTrigger("looksUp");
            this.bark();
        }
        yield return new WaitForSeconds(this.maxTimeWaitingForPick / 4);
        this.isPlaying = false;
        this.animator.ResetTrigger("looksDown");
        this.animator.SetTrigger("looksDown");
        this.animator.SetBool("isPlaying", false);
        this.StartCoroutine(this.waitForStandUp());
    }

    IEnumerator pickBallAndGoToPlayer() {
        this.transform.LookAt(this.playingTarget.transform.position);

        this.animator.ResetTrigger("isPicking");
        this.animator.SetTrigger("isPicking");
        yield return new WaitForSeconds(1.0f);
        this.ballInMouth = this.playingTarget;
        this.isSearching = false;
        this.attachBallToMouth(true);
        yield return new WaitForSeconds(0.5f);
        this.goToPlayer();
    }

    void attachBallToMouth(bool attach) {
        if (attach) {
            this.mouth.transform.eulerAngles = new Vector3(mouthOpened, 0, 0);
            this.ballInMouth.transform.SetParent(this.mouth.transform, true);
            this.ballInMouth.transform.localPosition = Vector3.zero;
            this.mouth.GetComponent<FixedJoint>().connectedBody = this.ballInMouth.GetComponent<Rigidbody>();
        } else {
            this.ballInMouth.transform.SetParent(null);
            this.mouth.transform.eulerAngles = new Vector3(defaultMouthOpening, 0, 0);
            this.mouth.GetComponent<FixedJoint>().connectedBody = null;
            this.ballInMouth = null;
            this.playingTarget = null;
        }
    }

    void giveBallAndWait() {
        this.transform.LookAt(this.player.GetComponent<Player>().feetPositionGuess);

        this.animator.ResetTrigger("Sits");
        this.animator.SetTrigger("Sits");
        this.isSitted = true;

        this.StartCoroutine(this.waitForPlayerPickOrThrow());
    }

    private float remap(float val, float in1, float in2, float out1, float out2) {
        return out1 + (val - in1) * (out2 - out1) / (in2 - in1);
    }

    private void OnApplicationQuit() {
        Weatavix.instance.OnInteractionStateChange -= this.catchEvent;
    }

    private void OnDestroy() {
        Weatavix.instance.OnInteractionStateChange -= this.catchEvent;
    }

    private void OnDisable() {
        Weatavix.instance.OnInteractionStateChange -= this.catchEvent;
    }
}
