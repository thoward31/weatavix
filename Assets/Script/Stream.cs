﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stream : MonoBehaviour
{
    public Transform particles;
    public Transform bottom;
    public Transform top;
    public float timeToDrop;
    private MeshRenderer meshRenderer;

    private void Awake() {
        this.meshRenderer = this.GetComponent<MeshRenderer>();
        this.meshRenderer.enabled = false;
        this.particles.GetComponent<ParticleSystem>().Stop();
    }

    public void startPouring() {
        this.meshRenderer.material.SetFloat("_Cutoff", 1);
        this.meshRenderer.enabled = true;
        this.StartCoroutine(this.alphaToFull());
        this.particles.position = this.bottom.position;
        this.particles.GetComponent<ParticleSystem>().Play();
    }

    public void stopPouring() {
        this.meshRenderer.enabled = false;
        this.particles.GetComponent<ParticleSystem>().Stop();
    }

    private void OnTriggerStay(Collider other) {
        float height = this.getHeight(other);
        if (height > this.particles.position.y) {
            this.updateStream(height);
        }
    }

    private void OnTriggerExit(Collider other) {
        this.updateStream(0);
    }

    private float getHeight(Collider collider) {
        return collider.transform.position.y;
    }

    private void updateStream(float newHeight) {
        Vector3 newPos = new Vector3(this.transform.position.x, newHeight, this.transform.position.z);
        this.particles.position = newPos;

        float top = this.top.position.y;
        float bot = this.bottom.position.y;

        float alpha = this.remap(newHeight, bot, top, 0, 1);

        this.meshRenderer.material.SetFloat("_Cutoff", alpha);
    }

    private float remap(float val, float in1, float in2, float out1, float out2) {
        return out1 + (val - in1) * (out2 - out1) / (in2 - in1);
    }

    IEnumerator alphaToFull() {
        float timeElapsed = 0.0f;

        while (timeElapsed < this.timeToDrop) {
            this.meshRenderer.material.SetFloat("_Cutoff", this.timeToDrop / timeElapsed);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        this.meshRenderer.material.SetFloat("_Cutoff", 0);
    }
}
